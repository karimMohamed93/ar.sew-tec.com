<?php
/**
 * Created by Sublime text 2.
 * User: thanhhiep992
 * Date: 12/08/15
 * Time: 10:00 AM
 */

if(!function_exists('s7upf_vc_service'))
{
    function s7upf_vc_service($attr,$content = false)
    {
        $html = $icon_html = '';
        extract(shortcode_atts(array(
            'style'       => 'icon',
            'image'       => '',
            'icon'        => '',
            'type'        => 'fontawesome',
            'icon_fontawesome'        => 'fa fa-adjust',
            'icon_openiconic'        => 'vc-oi vc-oi-dial',
            'icon_typicons'        => 'typcn typcn-adjust-brightness',
            'icon_entypo'        => 'entypo-icon entypo-icon-note',
            'icon_linecons'        => 'vc_li vc_li-heart',
            'title'       => '',
            'color'       => '',
            'des'       => '',
            'link'        => '',
        ),$attr));        
        switch ($style) {
            case 'image14':
                if(!empty($color)) $color = S7upf_Assets::build_css('color:'.$color);
                $html .=    '<div class="item-message-box table">
                                <div class="message-icon">
                                    <a href="'.esc_url($link).'" class="wobble-vertical">'.wp_get_attachment_image($image,'full').'</a>
                                </div>
                                <div class="message-text">
                                    <h3><a class="'.esc_attr($color).'" href="'.esc_url($link).'">'.esc_html($title).'</a></h3>
                                    <p class="desc">'.esc_html($des).'</p>
                                </div>
                            </div>';
                break;

            case 'image':
                $html .=    '<div class="item-service7">
                                <div class="service-icon">
                                    <a href="'.esc_url($link).'">'.wp_get_attachment_image($image,'full').'</a>
                                </div>
                                <div class="service-text">
                                    <h3 class="title14"><a href="'.esc_url($link).'">'.esc_html($title).'</a></h3>
                                    <p class="desc">'.esc_html($des).'</p>
                                </div>
                            </div>';
                break;

            case 'icon11':
                $iconClass = isset( ${'icon_' . $type} ) ? esc_attr( ${'icon_' . $type} ) : 'fa fa-adjust';
                if(!empty($icon)) $icon_html = '<i class="fa '.esc_attr($icon).'" aria-hidden="true"></i>';
                else $icon_html = '<span class="vc_icon_element-icon '.esc_attr($iconClass).'"></span>';
                $html .=    '<div class="item-service11">
                                <div class="item-service2 table border">
                                    <div class="service-icon"><a href="'.esc_url($link).'">'.$icon_html.'</a></div>
                                    <div class="service-info text-uppercase">
                                        <p><a href="'.esc_url($link).'">'.esc_html($title).'</a></p>
                                    </div>
                                </div>
                            </div>';
                break;

            case 'icon2':
                $iconClass = isset( ${'icon_' . $type} ) ? esc_attr( ${'icon_' . $type} ) : 'fa fa-adjust';
                if(!empty($icon)) $icon_html = '<i class="fa '.esc_attr($icon).'" aria-hidden="true"></i>';
                else $icon_html = '<span class="vc_icon_element-icon '.esc_attr($iconClass).'"></span>';
                $html .=    '<div class="item-service2 style2">
                                <div class="service-icon"><a href="'.esc_url($link).'">'.$icon_html.'</a></div>
                                <div class="service-info text-uppercase">
                                    <p><a href="'.esc_url($link).'">'.esc_html($title).'</a></p>
                                </div>
                            </div>';
                break;
            
            default:
                $iconClass = isset( ${'icon_' . $type} ) ? esc_attr( ${'icon_' . $type} ) : 'fa fa-adjust';
                if(!empty($icon)) $icon_html = '<i class="fa '.esc_attr($icon).'" aria-hidden="true"></i>';
                else $icon_html = '<span class="vc_icon_element-icon '.esc_attr($iconClass).'"></span>';
                $html .=    '<div class="item-service2 table border">
                                <div class="service-icon"><a href="'.esc_url($link).'">'.$icon_html.'</a></div>
                                <div class="service-info text-uppercase">
                                    <p><a href="'.esc_url($link).'">'.esc_html($title).'</a></p>
                                </div>
                            </div>';
                break;
        }       
        
        return $html;
    }
}

stp_reg_shortcode('s7upf_service','s7upf_vc_service');

vc_map( array(
    "name"      => esc_html__("SV Service", 'lucky'),
    "base"      => "s7upf_service",
    "icon"      => "icon-st",
    "category"  => '7Up-theme',
    "params"    => array_merge(s7upf_get_icon_params('style',array('icon','icon2','icon11')),array(
        array(
            "type" => "dropdown",
            "heading" => esc_html__("Style",'lucky'),
            "param_name" => "style",
            "value" => array(
                esc_html__("Icon",'lucky')  => 'icon',
                esc_html__("Icon 2",'lucky')  => 'icon2',
                esc_html__("Icon home 11",'lucky')  => 'icon11',
                esc_html__("Image",'lucky')  => 'image',
                esc_html__("Image home 14",'lucky')  => 'image14',
                ),
        ),
        array(
            "type" => "attach_image",
            "heading" => esc_html__("Image",'lucky'),
            "param_name" => "image",
            "dependency"    => array(
                "element"   => "style",
                "value"     => array("image","image14"),
                )
        ),
        array(
            "type" => "colorpicker",
            "heading" => esc_html__("Color title",'lucky'),
            "param_name" => "color",
            "dependency"    => array(
                "element"   => "style",
                "value"     => array("image14"),
                )
        ),
        array(
            "type" => "textfield",
            "holder" => "div",
            "heading" => esc_html__("Title",'lucky'),
            "param_name" => "title",
        ),
        array(
            "type" => "textfield",
            "heading" => esc_html__("Description",'lucky'),
            "param_name" => "des",
            "dependency"    => array(
                "element"   => "style",
                "value"     => array("image","image14"),
                )
        ),
        array(
            "type" => "textfield",
            "heading" => esc_html__("Link",'lucky'),
            "param_name" => "link",
        ),
    ))
));