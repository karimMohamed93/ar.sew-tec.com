<?php
/**
 * The template for displaying comments.
 *
 * The area of the page that contains both current comments
 * and the comment form.
 *
 * @package 7up-framework
 */

/*
 * If the current post is protected by a password and
 * the visitor has not yet entered the password we will
 * return early without loading the comments.
 */

if ( post_password_required() ) {
	return;
}

if(!function_exists('s7upf_comments_list'))
{ 
    function s7upf_comments_list($comment, $args, $depth) {

        $GLOBALS['comment'] = $comment;
        /* override default avatar size */
        $args['avatar_size'] = 60;
        if ('pingback' == $comment->comment_type || 'trackback' == $comment->comment_type) :
            ?>
            <li class="comment-pingback">
                <span id="comment-<?php comment_ID(); ?>" <?php comment_class(); ?>></span>
                <div class="comment-body">
                    <?php esc_html_e('Pingback:', 'lucky'); ?> <?php echo comment_author_link(); ?> <?php edit_comment_link(esc_html__('Edit', 'lucky'), '<span class="edit-link"><i class="fa fa-pencil-square-o"></i>', '</span>'); ?>
                </div>
            </li>
        <?php else : ?>
        	<li id="comment-<?php comment_ID(); ?>" <?php comment_class(empty($args['has_children']) ? '' : 'parent' ); ?>>
	        	<div class="item-comment">
					<div class="comment-thumb">
						<a class="avatar" href="<?php echo get_author_posts_url(get_the_author_meta('ID')); ?>"> <?php if (0 != $args['avatar_size']) echo get_avatar($comment, $args['avatar_size']); ?> </a>						
					</div>
					<div class="comment-info">
						<h3 class="comment-author"><a href="<?php echo get_author_posts_url(get_the_author_meta('ID')); ?>"> <?php printf(esc_html__('%s', 'lucky'), sprintf('%s', get_comment_author_link())); ?></a></h3>
						<p class="comment-time"><?php echo get_comment_time('F d, Y')?> <?php esc_html_e("at","lucky")?> <?php echo get_comment_time('g:i a')?></p>
						<div class="comment-text"><?php echo get_comment_text();?></div>
						<?php if (is_super_admin()): ?>
							<a href="<?php echo get_edit_comment_link ( get_comment_ID() ) ?>" class="comment-reply"><?php esc_html_e(' Edit','lucky');?></a>
						<?php endif; ?>
						<?php if (comments_open()): ?>
                            <?php echo str_replace('comment-reply-link', 'comment-reply', get_comment_reply_link(array_merge( $args, array('depth' => $depth, 'max_depth' => $args['max_depth'])))) ?>

                        <?php endif; ?>
					</div>
				</div>
			<!-- </li> -->
        <?php
        endif;
    }
}
?>
	<div class="post-comment">
		<div id="comments" class="comments-area comments">

			<?php // You can start editing here -- including this comment! ?>

			<?php if ( have_comments() ) : ?>
				<h2 class="post-title"><?php echo get_comments_number(); ?> <?php echo esc_html__('Comment to', 'lucky'); ?> &ldquo;<?php the_title()?>&rdquo;</h2>
		        <ol class="comments">
		            <?php
		            wp_list_comments(array(
		                'style' => '',
		                'short_ping' => true,
		                'avatar_size' => 74,
		                'max_depth' => '5',
		                'callback' => 's7upf_comments_list',
		                // 'walker' => new S7upf_custom_comment()
		            ));
		            ?>
		        </ol>

				<?php if ( get_comment_pages_count() > 1 && get_option( 'page_comments' ) ) : // are there comments to navigate through ?>
				<nav id="comment-nav-below" class="comment-navigation" role="navigation">
					<h1 class="screen-reader-text"><?php esc_html_e( 'Comment navigation', 'lucky' ); ?></h1>
					<div class="nav-previous"><?php previous_comments_link( esc_html__( '&larr; Older Comments', 'lucky' ) ); ?></div>
					<div class="nav-next"><?php next_comments_link( esc_html__( 'Newer Comments &rarr;', 'lucky' ) ); ?></div>
				</nav><!-- #comment-nav-below -->
				<?php endif; // check for comment navigation ?>

			<?php endif; // have_comments() ?>

			<?php
				// If comments are closed and there are comments, let's leave a little note, shall we?
				if ( ! comments_open() && '0' != get_comments_number() && post_type_supports( get_post_type(), 'comments' ) ) :
			?>
				<p class="no-comments"><?php esc_html_e( 'Comments are closed.', 'lucky' ); ?></p>
			<?php endif; ?>

		</div><!-- #comments -->
	</div>
	<div class="leave-comments post-comment-reply">
		<?php
		$comment_form = array(
            'title_reply' => esc_html__('Leave a comments', 'lucky'),
            'fields' => apply_filters( 'comment_form_default_fields', array(
                    'author' =>	'<div class="row"><div class="col-md-6 col-sm-6 col-xs-12">
                                    <p><input class="form-control input-md" id="author" name="author" type="text" value="' . esc_attr( $commenter['comment_author'] ) .'" placeholder="'.esc_html__( 'Your name*', 'lucky' ).'" /></p>
                                </div>',
                    'email' =>	'<div class="col-md-6 col-sm-6 col-xs-12">
                                    <p><input class="form-control input-md" id="email" name="email" type="text" value="' . esc_attr( $commenter['comment_author_email'] ) .'" placeholder="'.esc_html__( 'Your email*', 'lucky' ).'" /></p>
                                </div></div>',
                )
            ),
            'comment_field' =>  '<div class="form-group">
                                    <p><textarea id="comment" class="form-control" rows="5" name="comment" aria-required="true" placeholder="'.esc_html__( 'Your message*', 'lucky' ).'"></textarea></p>
                                </div>',
            'must_log_in' => '<div class="must-log-in control-group"><div class="controls"><p class="comment-note">'.esc_html__("Your email address will not be published. Required fields are marked *","lucky").'</p><p>' .sprintf(wp_kses_post(__( 'You must be <a href="%s">logged in</a> to post a comment.','lucky' )),wp_login_url( apply_filters( 'the_permalink', get_permalink() ) )) . '</p></div></div >',
            'logged_in_as' => '<div class="logged-in-as control-group"><div class="controls"><p class="comment-note">'.esc_html__("Your email address will not be published. Required fields are marked *","lucky").'</p><p>' .sprintf(wp_kses_post(__( 'Logged in as <a href="%1$s">%2$s</a>. <a href="%3$s" title="Log out of this account">Log out?</a>','lucky' )),admin_url( 'profile.php' ),$user_identity,wp_logout_url( apply_filters( 'the_permalink', get_permalink( ) ) )) . '</p></div></div>',
            'comment_notes_before' => '',
            'comment_notes_after' => '',
            'id_form'              => 'commentform',
            'id_submit'            => 'submit',
            'title_reply'          => esc_html__( 'Leave Comments','lucky' ),
            'title_reply_to'       => esc_html__( 'Leave a Reply %s','lucky' ),
            'cancel_reply_link'    => esc_html__( 'Cancel reply','lucky' ),
            'label_submit'         => esc_html__( 'Post comment','lucky' ),
            'class_submit'         => 'btn-link-default',
        );
		?>
		<?php comment_form($comment_form); ?>
	</div>
<?php

class S7upf_custom_comment extends Walker_Comment {
     
    /** START_LVL 
     * Starts the list before the CHILD elements are added. */
    function start_lvl( &$output, $depth = 0, $args = array() ) {       
        $GLOBALS['comment_depth'] = $depth + 1;

           $output .= '<div class="children">';
        }
 
    /** END_LVL 
     * Ends the children list of after the elements are added. */
    function end_lvl( &$output, $depth = 0, $args = array() ) {
        $GLOBALS['comment_depth'] = $depth + 1;
        $output .= '</div>';
    }
    function end_el( &$output, $object, $depth = 0, $args = array() ) {
    	$output .= '';
    }
}
?>

