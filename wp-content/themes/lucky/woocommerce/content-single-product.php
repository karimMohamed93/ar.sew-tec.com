<?php
/**
 * The template for displaying product content in the single-product.php template
 *
 * Override this template by copying it to yourtheme/woocommerce/content-single-product.php
 *
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

?>

<?php
	/**
	 * woocommerce_before_single_product hook
	 *
	 * @hooked wc_print_notices - 10
	 */
	 do_action( 'woocommerce_before_single_product' );

	 if ( post_password_required() ) {
	 	echo get_the_password_form();
	 	return;
	 }
?>
<div class="product-detail-content">
	<div id="product-<?php the_ID(); ?>" <?php post_class(); ?>>
		<div class="content-product-detail <?php echo s7upf_get_class_sidebar()?>">
			<?php s7upf_product_main_detai()?>
			<meta itemprop="url" content="<?php the_permalink(); ?>" />
			<?php 
				$tabs = apply_filters( 'woocommerce_product_tabs', array() );
			?>
			<div class="detail-product-tab">
				<div class="nav-tabs-default">
					<ul class="nav nav-tabs">
						<?php 
							$num=0;
							foreach ( $tabs as $key => $tab ) : 
							$num++;
						?>
								<li class="<?php if($num==1){echo 'active';}?>" role="presentation">
									<a href="<?php echo esc_url( '#sv-'.$key ); ?>" aria-controls="sv-<?php echo esc_attr( $key ); ?>" role="tab" data-toggle="tab"><?php echo apply_filters( 'woocommerce_product_' . $key . '_tab_title', esc_html( $tab['title'] ), $key ); ?></a>
								</li>
							
						<?php 
							endforeach; 
						?>			
						<li role="presentation"><a href="<?php echo esc_url('#tags')?>" aria-controls="tags" role="tab" data-toggle="tab"><?php esc_html_e("Tags","lucky")?></a></li>
						<?php 
							$custom_tab = get_post_meta(get_the_ID(),'product_tab_data',true);
							if(!empty($custom_tab) && is_array($custom_tab)){
								foreach ($custom_tab as $c_tab) {
									$tab_slug = str_replace(' ', '-', $c_tab['title']);
									$tab_slug = strtolower($tab_slug);
									echo '<li role="presentation"><a href="'.esc_url('#sv-'.$tab_slug).'" aria-controls="tags" role="tab" data-toggle="tab">'.$c_tab['title'].'</a></li>';
								}
							}
						?>
					</ul>
				</div>
				<div class="tab-content">
					<?php 
						$num=0;
						foreach ( $tabs as $key => $tab ) : 
						$num++;
					?>
						<div role="tabpanel" class="tab-pane <?php if($num==1){echo 'active';}?>" id="sv-<?php echo esc_attr( $key ); ?>">
							<div class="content-tags-detail">
								<?php call_user_func( $tab['callback'], $key, $tab ); ?>
							</div>
						</div>
					<?php endforeach; ?>				
					<div role="tabpanel" class="tab-pane" id="tags">
						<div class="content-tags-detail">
							<?php 
								global $product,$post;
								$tag_count = sizeof( get_the_terms( get_the_ID(), 'product_tag' ) );
								$tag_html = wc_get_product_tag_list( $product->get_id(), ', ', '<div class="tagged_as">' . _n( '', '', count( $product->get_tag_ids() ), 'lucky' ) . ' ', '</div>' );
								if($tag_html ) echo balanceTags($tag_html);
								else esc_html_e("No Tag","lucky");
							?>
						</div>
					</div>
					<?php 
						if(!empty($custom_tab) && is_array($custom_tab)){
							foreach ($custom_tab as $c_tab) {
								$tab_slug = str_replace(' ', '-', $c_tab['title']);
								$tab_slug = strtolower($tab_slug);
								echo '<div role="tabpanel" class="tab-pane" id="sv-'.$tab_slug.'">
										<div class="content-tags-detail">
											'.apply_filters('the_content',$c_tab['tab_content']).'
										</div>
									</div>';
							}
						}
					?>
				</div>
			</div>
		</div>
		<?php
			s7upf_single_product_tabs();
		?>
		<?php 
			$tabs = apply_filters( 'woocommerce_product_tabs', array() );
			// do_action( 'woocommerce_after_single_product_summary' );
		?>
	</div>	
	<?php do_action( 'woocommerce_after_single_product' ); ?>
</div>
