<?php
add_action( 'init', 'create_GroupCompanys' );
function create_GroupCompanys() {  
    $args = array(  
        'label' => __('GroupCompanys'),  
        'singular_label' => __('GroupCompany'),  
        'public' => true,  
        'show_ui' => true,  
        'capability_type' => 'post',  
        'hierarchical' => false,  
        'rewrite' => true,  
        'supports' => array('title', 'editor', 'thumbnail')  
       );  
   
    register_post_type('GroupCompanys',$args);  
    flush_rewrite_rules();

}
register_taxonomy("GroupCompany_group", array("GroupCompanys"), array("hierarchical" => true, "label" => "Categories", "singular_label" => "GroupCompanys group", "rewrite" => true)); 

 ?>